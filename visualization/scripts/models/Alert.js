var mongoose = require('mongoose');

var AlertSchema = new mongoose.Schema({
  timestamp: String,
  message: String,
  host: String,
  type: String
});

module.exports = mongoose.model('Alert', AlertSchema);
