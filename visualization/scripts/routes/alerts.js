var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Alert = require('../models/Alert.js');

/* GET /alerts listing. */
router.get('/', function(req, res, next) {
  Alert.find(function(err, alerts) {
    if (err) return next(err);

    res.json(alerts);
  }).sort({timestamp: 'descending'});
});

/* POST /alerts */
router.post('/', function(req, res, next) {
  console.log(req.body);
  Alert.create(req.body, function(err, post) {
    if (err) return next(err);

    res.json(post);
  });
});

/* GET /alerts/:id */
router.get('/:id', function(req, res, next) {
  Alert.findById(req.params.id, function(err, post) {
    if (err) return next(err);

    res.json(post);
  });
});

/* PUT /alerts/:id */
router.put('/:id', function(req, res, next) {
  Alert.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
    if (err) return next(err);

    res.json(post);
  });
});

/* DELETE /alerts/:id */
router.delete('/:id', function(req, res, next) {
  Alert.findByIdAndRemove(req.params.id, req.body, function(err, post) {
    if (err) return next(err);

    res.json(post);
  });
});

module.exports = router;
