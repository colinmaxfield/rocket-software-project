import consumers.KafkaConsumer;
import consumers.StreamConsumer;
import org.apache.log4j.PropertyConfigurator;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class AlertMonitor {

    private static final String DEFAULT_CONFIG = "config.properties";
    private static final String SPARK_PERIOD = "spark.period";
    private static final String CONFIG_KEYS = "Necessary Config Values: " +
            "\n" + SPARK_PERIOD;

    private static Properties prop = new Properties();

    public static void main(String[] args) throws Exception {
        PropertyConfigurator.configure(AlertMonitor.class.getClassLoader().getResource("conf/log4j.properties"));
        String filename = DEFAULT_CONFIG;
        if (args.length > 0) {
            filename = args[0];
        }

        // Try to read the properties file
        if (!loadProperties(filename)) {
            System.out.println(CONFIG_KEYS);
        }

        run();
    }

    /**
     * Run the Spark application to consume performance values from the Kafka server
     * and search for abnormalities. These should be sent to the database as alerts.
     *
     * @throws InterruptedException If the stream is interrupted throw an exception.
     */
    private static void run() throws InterruptedException {

        // Prepare the simple Spark configuration
        SparkConf sparkConf = new SparkConf()
                .setAppName("AlertMonitor")
                .setMaster("local[*]");

        // Set up the stream with the configured period in seconds
        int duration = Integer.parseInt(prop.getProperty(SPARK_PERIOD));
        JavaStreamingContext javaStreamingContext = new JavaStreamingContext(sparkConf, Durations.seconds(duration));

        // Consume the stream
        StreamConsumer consumer = new KafkaConsumer(prop);
        consumer.consumeStream(javaStreamingContext);

        // Submit the application
        javaStreamingContext.start();
        javaStreamingContext.awaitTermination();
    }

    /**
     * Try to read the properties file and check for all required properties.
     *
     * @param filename The location of the desired properties file.
     * @return boolean Whether or not properties file was found and valid.
     */
    private static boolean loadProperties(String filename) {
        try (InputStream input = new FileInputStream(filename)) {
            prop.load(input);
        } catch (IOException ex) {
            System.err.println("Unable to load configuration file: " + filename);
            ex.printStackTrace();
        }

        // Check for required properties
        if (!prop.containsKey(SPARK_PERIOD)) return false;

        return true;
    }
}
