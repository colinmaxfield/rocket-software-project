package interpreters;

import dto.Alert;
import dto.BasicMetrics;
import org.apache.log4j.Logger;

/**
 * A very simple class to check for abnormalities. This will look for
 * values that are outside of expected ranges and then potential issues
 * to create alerts for.
 */
public class SimpleChecker implements AlertOperation {

    private Logger log = Logger.getLogger(SimpleChecker.class);

    /**
     * This implementation will check that all values are in expected ranges
     * as well as possible issues to alert on.
     *
     * @param metrics The metrics for a host.
     * @return An alert object if there is one to send, else null.
     */
    @Override
    public Alert getAlert(BasicMetrics metrics) {

        Alert alert;

        alert = checkExpectedValues(metrics);
        if (alert != null) return alert;

        alert = checkAbnormalValues(metrics);
        return alert;
    }

    /**
     * Helper function to make sure that all values are within expected ranges.
     * That we don't have more free than total memory and that load is within 1.
     *
     * @param metrics The metrics to inspect.
     * @return The alert if there is one, else null.
     */
    private Alert checkExpectedValues(BasicMetrics metrics) {
        if (metrics.getFreeSwap() > metrics.getTotalSwap()) {
            return createAlert(metrics, "swap", "Total swap less than free swap, free="
                    + metrics.getFreeSwap() + ", total=" + metrics.getTotalSwap());
        } else if (metrics.getFreeMemory() > metrics.getTotalMemory()) {
            return createAlert(metrics, "mem", "Total memory less than free memory, free="
                    + metrics.getFreeMemory() + ", total=" + metrics.getTotalMemory());
        } else if (metrics.getCpuLoad() > 1 || metrics.getCpuLoad() < 0) {
            return createAlert(metrics, "load", "Load outside of normal values, load=" + metrics.getCpuLoad());
        }

        return null;
    }

    /**
     * Search for possible issues to create an alert about. If the system is in swap, is using too much
     * memory, or has a high load.
     *
     * @param metrics The metrics to inspect.
     * @return The alert if there is one, else null.
     */
    private Alert checkAbnormalValues(BasicMetrics metrics) {
        double usedSwap = 1 - (double) metrics.getFreeSwap() / (double) metrics.getTotalSwap();
        if (usedSwap > 0) {
            return createAlert(metrics, "swap", String.format("System is into swap, used=%2.2f%%", usedSwap * 100));
        }

        double usedMem = 1 - (double) metrics.getFreeMemory() / (double) metrics.getTotalSwap();
        if (usedMem > 0.75) {
            return createAlert(metrics, "mem", String.format("System is using a lot of available memory, used=%2.2f%%", usedMem * 100));
        }

        if (metrics.getCpuLoad() > 0.75) {
            return createAlert(metrics, "load", String.format("System load is high, load=%2.2f%%", metrics.getCpuLoad() * 100));
        }

        return null;
    }

    /**
     * Helper function to create an alert.
     *
     * @param metrics The metrics object to get the timestamp and host from.
     * @param type The type of alert that was found. Usually swap, mem, or load.
     * @param message The message to explain what the alert is about.
     * @return Alert The new alert object to send.
     */
    private Alert createAlert(BasicMetrics metrics, String type, String message) {
        return new Alert(metrics.getTimestamp(), type, metrics.getHost(), message);
    }
}
