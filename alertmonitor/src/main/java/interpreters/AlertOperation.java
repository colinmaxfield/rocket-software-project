package interpreters;

import dto.Alert;
import dto.BasicMetrics;

/**
 * Allows us to look at basic metrics and search for any abnormalities
 * from which we can create an alert.
 */
public interface AlertOperation {

    /**
     * Given a specific set of metrics, check to see if there is anything
     * abnormal that we need to create an alert for.
     *
     * @param metrics The metrics for a host
     * @return Alert A new alert if the readings are abnormal, else null
     */
    Alert getAlert(BasicMetrics metrics);
}
