package persistence;

import com.google.gson.Gson;
import dto.Alert;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;

public class AlertServerRequest {

    private Logger log = Logger.getLogger(AlertServerRequest.class);

    private String serverURL = "http://localhost:3000";
    private HttpClient client = HttpClientBuilder.create().build();

    /**
     * Creates a new alert server object to send alerts on
     * for persistence.
     */
    public AlertServerRequest() {
    }

    /**
     * Send the alert to the given server as a POST.
     *
     * @param alert The alert object to be sent.
     */
    public void sendAlert(Alert alert) {

        try {
            HttpPost request = new HttpPost(serverURL + "/alerts");
            StringEntity params = new StringEntity(new Gson().toJson(alert));
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = client.execute(request);
        } catch (IOException ex) {
            log.error("Unable to make request", ex);
        }
    }
}
