package consumers;

import com.google.gson.Gson;
import dto.Alert;
import dto.BasicMetrics;
import interpreters.SimpleChecker;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction2;
import org.apache.spark.streaming.Time;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import persistence.AlertServerRequest;
import scala.Tuple2;

import java.util.*;

/**
 * A consumer to receive the Kafka stream and operate on it. In
 * this case we will look for unusual performance values.
 */
public class KafkaConsumer implements StreamConsumer {

    private static Logger log = Logger.getLogger(KafkaConsumer.class);

    private static final String BOOTSTRAP_SERVERS = "bootstrap.servers";
    private static final String KAFKA_TOPIC = "kafka.topic";
    private static final String DEFAULT_BOOTSTRAP = "localhost:9092";
    private static final String DEFAULT_TOPIC = "test";

    private Properties prop;

    /**
     * Creates a KafkaConsumer with all of the necessary config values
     * to receive data from the given Kafka server.
     *
     * @param prop The properties object with necessary config values.
     */
    public KafkaConsumer(Properties prop) {
        this.prop = prop;
    }

    /**
     * Consume the spark stream that is from Kafka. This will look at
     * each metric and try to find abnormalities. If there are any it will
     * send an alert to the alert server.
     *
     * @param streamingContext The streaming context for Spark.
     */
    @Override
    public void consumeStream(JavaStreamingContext streamingContext) {

        // Set up the topic and parameters to read from Kafka
        String topic = prop.getProperty(KAFKA_TOPIC, DEFAULT_TOPIC);
        Set<String> topicsSet = new HashSet<>(Collections.singleton(topic));
        Map<String, Object> kafkaParams = getParameters();

        // Begin consuming from Kafka
        final JavaInputDStream<ConsumerRecord<String, String>> stream = KafkaUtils
                .createDirectStream(streamingContext,
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.<String, String>Subscribe(topicsSet, kafkaParams));

        // Convert record to key and value
        stream.mapToPair((PairFunction<ConsumerRecord<String, String>, String, String>)
                record -> new Tuple2<>(record.key(), record.value()));

        // Check each performance reading for abnormalities and send alert if necessary
        stream.foreachRDD((VoidFunction2<JavaRDD<ConsumerRecord<String, String>>, Time>) (rdd, time) -> {
            log.error("New RDD of count: " + rdd.count());
            rdd.foreach(record -> {
                BasicMetrics metrics = new Gson().fromJson(record.value(), BasicMetrics.class);

                Alert alert = new SimpleChecker().getAlert(metrics);
                if (alert != null) {
                    new AlertServerRequest().sendAlert(alert);
                }
            });
        });
    }

    /**
     * Create the necessary Kafka parameters to be able to receive performance
     * values from the server.
     *
     * @return Map The Kafka parameters including servers, group, and deserializers.
     */
    private Map<String, Object> getParameters() {

        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", prop.getProperty(BOOTSTRAP_SERVERS, DEFAULT_BOOTSTRAP));
        kafkaParams.put("group.id", "2");
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);

        return kafkaParams;
    }
}
