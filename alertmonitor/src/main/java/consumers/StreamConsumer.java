package consumers;

import org.apache.spark.streaming.api.java.JavaStreamingContext;

/**
 * The StreamConsumer allows us to switch out how we consume the Spark
 * stream depending on what we want to do.
 */
public interface StreamConsumer {

    /**
     * Given a specific streaming context, consume it as needed for
     * the implementation.
     *
     * @param streamingContext The streaming context for Spark.
     */
    void consumeStream(JavaStreamingContext streamingContext);
}
