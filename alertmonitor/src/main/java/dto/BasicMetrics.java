package dto;

/**
 * A simple dto to represent a few performance values from a computer. Included
 * is the free and total physical memory, the free and total swap memory, and
 * the system load.
 */
public class BasicMetrics {

    private long freeMemory = 0;
    private long totalMemory = 0;
    private long freeSwap = 0;
    private long totalSwap = 0;
    private double cpuLoad = 0.0;
    private String timestamp = "";
    private String host = "";

    /**
     * Constructs a BasicMetrics object with all members set. All values should be from a
     * single reading of the computer.
     *
     * @param freeMemory    The amount of available physical memory
     * @param totalMemory   The total amount of physical memory in the system, should be
     *                          more than the free memory
     * @param freeSwap      The amount of available swap memory
     * @param totalSwap     The total amount of swap memory in the system, should be
     *                          more than the free swap memory
     * @param cpuLoad       The system cpu load between 0 and 1
     * @param timestamp     The current timestamp
     * @param host          The current host name of the computer
     */
    public BasicMetrics(long freeMemory, long totalMemory, long freeSwap,
                        long totalSwap, double cpuLoad, String timestamp, String host) {
        this.freeMemory = freeMemory;
        this.totalMemory = totalMemory;
        this.freeSwap = freeSwap;
        this.totalSwap = totalSwap;
        this.cpuLoad = cpuLoad;
        this.timestamp = timestamp;
        this.host = host;
    }

    public long getFreeMemory() {
        return freeMemory;
    }

    public void setFreeMemory(long freeMemory) {
        this.freeMemory = freeMemory;
    }

    public long getTotalMemory() {
        return totalMemory;
    }

    public void setTotalMemory(long totalMemory) {
        this.totalMemory = totalMemory;
    }

    public long getFreeSwap() {
        return freeSwap;
    }

    public void setFreeSwap(long freeSwap) {
        this.freeSwap = freeSwap;
    }

    public long getTotalSwap() {
        return totalSwap;
    }

    public void setTotalSwap(long totalSwap) {
        this.totalSwap = totalSwap;
    }

    public double getCpuLoad() {
        return cpuLoad;
    }

    public void setCpuLoad(double cpuLoad) {
        this.cpuLoad = cpuLoad;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "BasicMetrics{" +
                "freeMemory=" + freeMemory +
                ", totalMemory=" + totalMemory +
                ", freeSwap=" + freeSwap +
                ", totalSwap=" + totalSwap +
                ", cpuLoad=" + cpuLoad +
                ", timestamp='" + timestamp + '\'' +
                ", host='" + host + '\'' +
                '}';
    }
}
