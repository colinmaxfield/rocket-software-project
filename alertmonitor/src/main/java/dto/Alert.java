package dto;

/**
 * A dto to represent a performance alert that was detected. It contains the information
 * needed to understand what, when and where it happened.
 */
public class Alert {

    private String timestamp = "";
    private String type = "";
    private String host = "";
    private String message = "";

    /**
     * Constructs an Alert with all members set that is ready to be saved to
     * a database for persistence.
     *
     * @param timestamp The timestamp for the alert in format "yyyy:MM:dd HH:mm:ss".
     * @param type The type of alert that was found.
     * @param host The host of where the alert happened.
     * @param message A longer message to explain what happened.
     */
    public Alert(String timestamp, String type, String host, String message) {
        this.timestamp = timestamp;
        this.type = type;
        this.host = host;
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Alert{" +
                "timestamp='" + timestamp + '\'' +
                ", type='" + type + '\'' +
                ", host='" + host + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
