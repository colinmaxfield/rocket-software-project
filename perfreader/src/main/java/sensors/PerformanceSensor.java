package sensors;

import com.sun.management.OperatingSystemMXBean;
import dto.BasicMetrics;
import org.apache.edgent.function.Supplier;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * A PerformanceSensor is an implementation of an Edgent Supplier that
 * can be used to get the current performance values of the host system
 * on demand. Values include: current freeMemory, totalMemory,
 * freeSwap, totalSwap, and system cpuLoad.
 */
public class PerformanceSensor implements Supplier<BasicMetrics> {

    private static final SimpleDateFormat TIME_STYLE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final String TEST_MODE = "mode.test";

    private OperatingSystemMXBean osBean;
    private boolean testMode = false;
    private int testCounter = 0;

    /**
     * Creates a basic PerformanceSensor that is ready to read the performance
     * of the host system.
     */
    public PerformanceSensor(Properties prop) {
        osBean = (OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
        if ("true".equals(prop.getProperty(TEST_MODE))) {
            testMode = true;
        }
    }

    /**
     * Implementation of the Supplier.get method that will read all necessary
     * performance information and return it.
     *
     * @return dto.BasicMetrics object containing the current freeMemory, totalMemory,
     * freeSwap, totalSwap, and system cpuLoad values of the host system.
     */
    @Override
    public BasicMetrics get() {
        long freeMemory = osBean.getFreePhysicalMemorySize();
        long totalMemory = osBean.getTotalPhysicalMemorySize();
        long freeSwap = osBean.getFreeSwapSpaceSize();
        long totalSwap = osBean.getTotalSwapSpaceSize();
        double cpuLoad = osBean.getSystemCpuLoad();
        String timestamp = TIME_STYLE.format(new Date());
        String host;
        try {
            host = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            host = "unknown";
        }

        if (testMode) {
            testCounter++;
            if (testCounter == 10) {
                freeMemory = 15;
            } else if (testCounter == 20) {
                freeSwap = 100000;
            } else if (testCounter == 30) {
                cpuLoad = 0.85;
            } else if (testCounter == 40) {
                totalMemory = 1000;
            } else if (testCounter == 50) {
                totalSwap = 1000;
            } else if (testCounter == 60) {
                cpuLoad = 1.5;
            } else if (testCounter == 70) {
                testCounter = 0;
            }
        }

        return new BasicMetrics(freeMemory, totalMemory, freeSwap, totalSwap, cpuLoad, timestamp, host);
    }
}
