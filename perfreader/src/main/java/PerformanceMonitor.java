import dto.BasicMetrics;
import org.apache.log4j.BasicConfigurator;
import publishers.KafkaPublisher;
import sensors.PerformanceSensor;
import org.apache.edgent.providers.direct.DirectProvider;
import org.apache.edgent.topology.TStream;
import org.apache.edgent.topology.Topology;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * A simple application that uses Apache Edgent to read the host system's
 * performance and publish it to a Kafka server.
 */
public class PerformanceMonitor {

    private static final String DEFAULT_CONFIG = "config.properties";
    private static final String BOOTSTRAP_SERVERS = "bootstrap.servers";
    private static final String ZOOKEEPER_CONNECT = "zookeeper.connect";
    private static final String KAFKA_TOPIC = "kafka.topic";
    private static final String CONFIG_KEYS = "Necessary Config Values: " +
            "\n" + BOOTSTRAP_SERVERS +
            "\n" + ZOOKEEPER_CONNECT +
            "\n" + KAFKA_TOPIC;

    private static Properties prop = new Properties();

    /**
     * The performance monitor will start Edgent to read the system and will send it to
     * a Kafka server indefinitely.
     *
     * @param args Command line arguments, there only needs to be the properties filename if not
     *             using the default config.
     */
    public static void main(String[] args) {
        BasicConfigurator.configure();

        String filename = DEFAULT_CONFIG;
        if (args.length > 0) {
            filename = args[0];
        }

        // Try to read the properties file
        if (!loadProperties(filename)) {
            System.out.println(CONFIG_KEYS);
        }

        run();
    }

    /**
     * Start the Edgent readings and publish the information.
     */
    private static void run() {
        DirectProvider directProvider = new DirectProvider();
        Topology topology = directProvider.newTopology("kafkaPerformancePublisher");

        // Poll the system's performance every second
        TStream<BasicMetrics> performanceReadings = topology.poll(new PerformanceSensor(prop), 1L, TimeUnit.SECONDS);

        // Create and configure the KafkaPublisher to publish
        Map<String, Object> config = createConfig();
        KafkaPublisher<BasicMetrics> kafka = new KafkaPublisher<>(topology, config, prop.getProperty(KAFKA_TOPIC));

        // Publish the performance stream to Kafka
        kafka.publish(performanceReadings);

        // Submit the topology and begin to send messages
        directProvider.submit(topology);
    }

    /**
     * Create the Map for the publisher to use in its configuration.
     *
     * @return Map The configuration map to ensure the publisher is setup correctly
     */
    private static Map<String, Object> createConfig(){
        Map<String, Object> config = new HashMap<>();

        config.put(BOOTSTRAP_SERVERS, prop.getProperty(BOOTSTRAP_SERVERS));
        config.put(ZOOKEEPER_CONNECT, prop.getProperty(ZOOKEEPER_CONNECT));

        return config;
    }

    /**
     * Try to read the properties file and check for all required properties.
     *
     * @param filename The location of the desired properties file
     * @return boolean Whether or not properties file was found and valid
     */
    private static boolean loadProperties(String filename) {
        try (InputStream input = new FileInputStream(filename)) {
            prop.load(input);
        } catch (IOException ex) {
            System.err.println("Unable to load configuration file: " + filename);
            ex.printStackTrace();
        }

        // Check for required properties
        if (!prop.containsKey(BOOTSTRAP_SERVERS)) return false;
        if (!prop.containsKey(ZOOKEEPER_CONNECT)) return false;
        if (!prop.containsKey(KAFKA_TOPIC)) return false;

        return true;
    }
}
