package publishers;

import org.apache.edgent.topology.TStream;

/**
 * Basic type of publisher to allow easily switching out
 * how to publish the Edgent stream.
 */
public interface Publisher<T> {

    /**
     * This is the publish method that is required for a publisher. Each
     * implementation of this method must contain an Edgent sink.
     *
     * @param stream The Edgent stream to publish.
     */
    void publish(TStream<T> stream);
}
