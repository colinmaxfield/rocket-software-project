package publishers;

import com.google.gson.Gson;
import org.apache.edgent.connectors.kafka.KafkaProducer;
import org.apache.edgent.topology.TStream;
import org.apache.edgent.topology.Topology;

import java.util.Map;

/**
 * A publisher implementation to act as a Kafka producer.
 */
public class KafkaPublisher<T> implements Publisher<T> {

    private String topic;
    private KafkaProducer kafka;

    /**
     * Creates a KafkaPublisher that is ready to publish new streams.
     *
     * @param topology The Edgent topology to use for the streams.
     * @param config Any key-value pairs needed to configure the Kafka connection.
     * @param topic The topic to publish to.
     */
    public KafkaPublisher(Topology topology, Map<String, Object> config, String topic) {
        if (topic != null) {
            this.topic = topic;
        }

        kafka = new KafkaProducer(topology, () -> config);
    }

    /**
     * This will publish the given stream to the kafka server
     * that was given in the configuration.
     *
     * @param stream The Edgent stream to publish.
     */
    @Override
    public void publish(TStream<T> stream) {
        kafka.publish(stream.map(e -> new Gson().toJson(e)), topic);
    }
}
